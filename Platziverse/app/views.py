from django.shortcuts import render
from django.http import HttpResponse
from django.http import QueryDict
# Create your views here.
from datetime import datetime
import json

def hello(request):
    return HttpResponse('Oh hi, arrive friday yeah is {now}'.format(
        now=datetime.now().strftime(
            '%b %dth, %Y - %H:%M hrs'
            )
    ))

def hi(request):
    data = '{1,22,32,4,36,84,37}'
    sorted_ints = sorted(data)
    data2 = {
        'status': 'ok',
        'data': sorted_ints,
        'message': 'Integers sorted successfully!'
    }

    return HttpResponse(
        json.dumps(data2, indent=4),
         content_type='application/json'
         )